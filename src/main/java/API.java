import org.apache.commons.codec.binary.Hex;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class API {
    private String Key ; // your Key
    private String Secret ;// your secret
    private String URL ; // website Url

    API(){}// empty constructor

    API(String Key , String Secret , String Url){
        /*
         * this constructor take three values
         * this constructor used if you need to do a HTTP POST REQUEST
         * Key = your Key
         * Secret = your secret
         * URL = website Url
         * */
        setKey(Key);
        setSecret(Secret);
        setURL(Url);
    }

    API(String Url){
        /*
         * this constructor take one value
         * this constructor used if you need to do a HTTP GET REQUEST
         * Url =  website Url
         */
        setURL(Url);
    }

    public void  setKey(String key){
        this.Key = key;
    }

    public void setSecret(String secret){
        this.Secret = secret;
    }

    public void setURL(String Url){
        this.URL = Url;
    }

    public final String getKey(){
        return this.Key;
    }

    public final  String getSecret(){
        return this.Secret;
    }

    public final String getURL(){
        return this.URL;
    }

    public final  String getBalances() throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        /*
         * this function Fetch the Balance of each currency
         * return String of json object
         * */

        String nonce = String.valueOf(System.currentTimeMillis()*99999999) ;
        String query = "command=returnBalances&nonce=" + nonce;

        Mac shaMac = Mac.getInstance("HmacSHA512");
        SecretKeySpec keySpec = new SecretKeySpec(this.getSecret().getBytes(), "HmacSHA512");
        shaMac.init(keySpec);
        final byte[] macData = shaMac.doFinal(query.getBytes());
        String sign = Hex.encodeHexString(macData);
        //System.out.println(sign);

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost post = new HttpPost(this.getURL());
        post.addHeader("Key", this.getKey());
        post.addHeader("Sign", sign);

        List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("command", "returnBalances"));
        params.add(new BasicNameValuePair("nonce", nonce));
        post.setEntity(new UrlEncodedFormEntity(params));

        CloseableHttpResponse response = httpClient.execute(post);
        HttpEntity responseEntity = response.getEntity();
        //System.out.println(response.getStatusLine());


        String result = EntityUtils.toString(responseEntity) ;

        return result;
    }

    public String getCurrenciesPairs() throws IOException {

        String result = "";

        URL u = new URL(this.getURL());
        HttpURLConnection con = (HttpURLConnection) u.openConnection();
        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(
                con.getInputStream()
        ));

        String s;
        StringBuffer sr = new StringBuffer();

        while ((s = in.readLine()) !=null){
            result = result + s;
        }
        in.close();

        return result;
    }
}