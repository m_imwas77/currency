import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.math.BigDecimal;

/*
 * this class take a json object and pair name and find all data of this currency pair
 * */

public class CurrenciesPairs {
    private  String data; // Json Object
    private  String pairName; // currency pair name
    CurrenciesPairs(){} // empty constructor

    CurrenciesPairs(String data,String pairName){
        // this constructor take tow value data = Json Object pairName = currency pair name
        setData(data);
        setPairName(pairName);
    }

    public void setData(String data){
        this.data = data;
    }

    public final  String getData(){
        return this.data;
    }

    public void setPairName(String pairName){
        this.pairName = pairName;
    }

    public final  String getPairName(){
        return this.pairName;
    }

    private JsonObject getJsonObject(){
        /*
         * parse data to Json Object
         * return json Object
         * */
        JsonParser parser = new JsonParser();
        JsonObject objects = (JsonObject) parser.parse(this.getData());
        JsonObject object = (JsonObject) objects.get(this.getPairName());
        return  object;
    }

    public long  getPairId(){
        /*
         * return currency pair id
         *
         * */
        JsonElement object =  this.getJsonObject().get("id");
        return object.getAsLong();
    }

    public  BigDecimal getLast(){
        /*
         * return currency pair
         *
         * */
        JsonElement object = this.getJsonObject().get("last");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public  BigDecimal getLowestAsk(){
        /*
         * return currency pair LowestAsk
         *
         * */
        JsonElement object = this.getJsonObject().get("lowestAsk");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public  BigDecimal getHighestBid(){
        /*
         * return currency pair HighestBid
         *
         * */
        JsonElement object = this.getJsonObject().get("highestBid");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public  BigDecimal getPercentChange	(){
        /*
         * return currency PercentChange
         *
         * */
        JsonElement object = this.getJsonObject().get("percentChange");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public  BigDecimal  getBaseVolume(){
        /*
         * return currency BaseVolume
         *
         * */
        JsonElement object = this.getJsonObject().get("baseVolume");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public BigDecimal getQuoteVolume(){
        /*
         * return currency pair QuoteVolume
         *
         * */
        JsonElement object = this.getJsonObject().get("quoteVolume");
        return object.getAsBigDecimal();
    }

    public  BigDecimal getHigh24hr(){
        /*
         * return currency pair High24hr
         *
         * */
        JsonElement object = this.getJsonObject().get("high24hr");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public  BigDecimal getLow24hr(){
        /*
         * return currency pair Low24hr
         *
         * */
        JsonElement object = this.getJsonObject().get("low24hr");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public boolean isFrozen(){
        /*
         * return currency pair Frozen or not
         *
         * */
        JsonElement object = this.getJsonObject().get("isFrozen");
        return (boolean) object.getAsBoolean();
    }

}
