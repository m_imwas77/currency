import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

import static java.lang.System.out;

public class Main {
    public final static long EVERY_MINUTE = 60000;
    public final static long EVERY_HALF_MINUTE = 30000;
    public final static long EVERY_QUARTER_MINUTE = 20000;
    public final static String key = "UUZKVYEH-7LP65S7Y-7DFOZVPM-IUOEEQRY";// yor key
    public final static String secret = "982f53bef8d252f0d0f659d06bf0b05578c34bc964194ae43fe40b5408b997a5dd75fb359fec5b1b087821cb7ad9fe6a28774b10a47d94f3b08a9af0221107be";
    // yor secret
    public final static String url = "https://poloniex.com/tradingApi";
    // web site url
    public static void main(String args[]) throws InterruptedException {
        printBalance();
        while (true) {
            try {
                // connect to psotgresql driver
                Class.forName("org.postgresql.Driver");
            } catch (Exception e) {
                out.println(e.getMessage());
            }
            try {
                // connect to DATABASES
                Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DB?user=imwas&password=2151997mohm");
                PreparedStatement ps = conn.prepareStatement("CREATE TABLE IF NOT EXISTS pair(id SERIAL NOT NULL PRIMARY KEY," +
                        "pair_name text NOT NULL," +
                        "pair_id numeric  NOT NULL ," +
                        "last numeric  NOT NULL ," +
                        "lowestAsk numeric NOT NULL ," +
                        "highestBid numeric NOT NULL ," +
                        "percentChange numeric NOT NULL ," +
                        "baseVolume numeric NOT NULL ," +
                        "isFrozen text NOT NULL ," +
                        "high24hr numeric NOT NULL ," +
                        "low24hr numeric NOT NULL)");
                ps.executeUpdate();
                ps.close();
                String Tiker = getTiker();
                for (int i = 0; i < Names.CurrencyPairsName.length; i++) {
                    // insert to DATABASES
                    PreparedStatement ps2 = conn.prepareStatement(getQuery(Tiker, Names.CurrencyPairsName[i]));
                    ps2.executeUpdate();
                    ps2.close();
                    // if there is a change more then 3 % show a message
                    CurrenciesPairs currenciesPairs = new CurrenciesPairs(Tiker, Names.CurrencyPairsName[i]);
                    if (Double.parseDouble(currenciesPairs.getPercentChange().toString()) > 3) {
                        out.println(Names.CurrencyPairsName[i] + "has changes " + currenciesPairs.getPercentChange());
                    }
                }
            } catch (Exception e) {
                out.println(e.getMessage());
            }
            String Tiker = getTiker();
            for (int i = 0; i < Names.CurrencyPairsName.length; i++) {

            }
            out.println("*******************************************************************************************************");
            Thread.sleep(EVERY_QUARTER_MINUTE);
        }
    }
    public static String getTiker(){
        // return all data of Currency Pair
        API api1 = new API("https://poloniex.com/public?command=returnTicker");
        try {
            return api1.getCurrenciesPairs();
        }catch (IOException e){
            out.println(e.getMessage());
        }
        return null;
    }
    public static  String getBalance(){
        // return balance  of each currency
        API api = new API(key,secret,url);
        try{
            return api.getBalances();
        }catch (NoSuchAlgorithmException e){
            out.println(e.getMessage());
        }catch (InvalidKeyException e){
            out.println(e.getMessage());
        }catch (IOException e){
            out.println(e.getMessage());
        }
        return null;
    }
    public static String getQuery(String data , String name){
        // create insert Query
        CurrenciesPairs currenciesPairs = new CurrenciesPairs(data,name);
        String Query = "VALUES ("+"\'"+name+"\'"+","+currenciesPairs.getPairId()+","
                +currenciesPairs.getLast()+","+
                currenciesPairs.getLowestAsk()+
                ","+currenciesPairs.getHighestBid()
                +","+currenciesPairs.getPercentChange()+","+
                currenciesPairs.getBaseVolume()
                +","+currenciesPairs.isFrozen()+","
                +currenciesPairs.getHigh24hr()+","+currenciesPairs.getLow24hr()+")";
        Query = "INSERT INTO pair(pair_name, pair_id, last, lowestask, highestbid, percentchange, basevolume, isfrozen, high24hr, low24hr)"+
                Query;
        return Query;
    }
    public static void printBalance(){
        // convert balance ot json object
        // print balance
        String balance = getBalance();
        JsonParser parser = new JsonParser();
        JsonObject object = (JsonObject)  parser.parse(balance);
        out.println(object);
        for(int i=0;i<Names.CurrencyName.length;i++){
            out.println(Names.CurrencyName[i] + " = " + object.get(Names.CurrencyName[i]));
        }
    }
}
